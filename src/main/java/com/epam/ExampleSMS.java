package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC4cd06e9d9e4c18a16e332ec494519a90";
    public static final String AUTH_TOKEN = "dc542df247ef7006b58096b33e23cabe";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380964695677"), /*my phone number*/
                        new PhoneNumber("+12158834843"), str).create(); /*attached to me number*/

    }
}
